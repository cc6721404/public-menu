fetch('https://api.menyit.se/v1/menus', {
    headers: {'X-API-Key': 'ID0hwtGk7mPoxL99F8WJZM19EeQ5wpzGQc/S1cLpWFU='}
})
    .then((res) => res.json())
    .then((menus) => {
        menus.map(menu => {
            generateCategory(menu);
            menu.items.map(item => generateCategoryItem(item, menu));
        });

        createListeners();
    });

function generateCategory(menu) {
    let categoriesContainer = document.querySelector('.categories ul');
    categoriesContainer.innerHTML += `<li data-id="${menu.id}">${menu.title}</li>`;
}

function generateCategoryItem(item, menu) {
    let foodContainer = document.querySelector('.item-container');
    foodContainer.innerHTML += `<div class="col-md-6 item ${menu.id}">
                        <div class="menu-item">
                            <div class="menu-item-image">
                                <img src="${item.imageUrl ?? '/images/placeholder.jpg'}" alt="${item.title}">
                            </div>
                            <div class="menu-item-info">
                                <div class="header">  
                                    <span class="title">${item.title}</span>
                                    <span class="price">${item.price ?? ''}</span>
                                </div>
                                <div class="description">${item.description ?? ''}</div>
                            </div>   
                        </div>
                    </div>`
}

function createListeners() {
    const item = document.querySelectorAll('.item');
    const menu = document.querySelectorAll('ul');

    menu.forEach(m => {
        m.addEventListener('click', e => {
            item.forEach(box => {
                box.classList.add('d-none')
                if (e.target.dataset.id === undefined) {
                    box.classList.remove('d-none')
                } else {
                    if (box.classList.contains(e.target.dataset.id)) {
                        box.classList.remove('d-none')
                    }
                }
            })
        })
    });
}

